const URL_TO_JSON = '/gemotest-map/js/placemarks.json';

ymaps.ready(init);

let map;

function init () {
    const minZoom = document.querySelector('#map').clientWidth > 414 ? 2 : 1;
    map = new ymaps.Map('map', {
        center: [55.250572, 73.596028],
        zoom: minZoom + 1,
        controls: ['zoomControl', 'fullscreenControl']
    }, {
        minZoom,
    }
);

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
            map.setCenter([position.coords.latitude, position.coords.longitude], 11);
        })
    }

    const BALLOON_LAYOUT = ymaps.templateLayoutFactory.createClass(
        '<div class="balloon">' +
            '<h1 class="balloon__title">Гемотест</h1>' +
            '<div class="address">' +
                '<div class="address__main">{{ properties.description.city }}</div>' +
                '<div class="address__main">{{ properties.description.address }}</div>' +
                '<div class="address__metro">' +
                '{% for key, metro in properties.description.metro %}' +
                    '{% if key > 0 %}' +
                        '<br>' +
                    '{% endif %}' +
                    '{{ metro }}' +
                '{% endfor %}' +
                '</div>' +
            '</div>' +
            '<div class="features">' +
                '{% for feature in properties.description.features %}' +
                '<div class="feature {{ feature.type }}">' +
                    '<h3 class="feature__title">{{ feature.title }}</h3>' +
                    '<div class="feature__price">{{ feature.price|raw }}</div>' +
                    '<div class="feature__hours">{{ feature.time|raw }}</div>' +
                    '<a href="{{ feature.url|raw }}" class="feature__button">Оформить заказ</a>' +
                '</div>' +
                '{% endfor %}' +
                '<div class="feature">' +
                    '<h3 class="feature__caption">{{ properties.description.caption|raw }}</h3>' +
                    '<div class="feature__hours">{{ properties.description.others|raw }}</div>' +
                '</div>' +
            '</div>' +
        '</div>'
    );

    const objManager = new ymaps.ObjectManager();

    fetchGeoData().then(data => {
        objManager.add(data);
        objManager.objects.options.set('balloonContentLayout', BALLOON_LAYOUT);
        objManager.objects.options.set('preset', 'islands#darkGreenDotIcon');
        objManager.objects.options.set('balloonPanelMaxMapArea', 304704);
        objManager.objects.options.set('balloonMaxHeight', '450');
        objManager.objects.options.set('balloonPanelMaxHeightRatio', '.6');
        objManager.objects.options.set('iconLayout', 'default#image');
        objManager.objects.each(object => {
            objManager.objects.setObjectOptions(object.id, {
                iconImageHref: `images/mark-${object.properties.description.isUrgent ? 'urgent' : 'standard'}.svg`
            });
        });
        map.geoObjects.add(objManager);
    });

}

function fetchGeoData() {
    return fetch(URL_TO_JSON, {
        headers: {
            'Content-Type': 'application/json'
        },
    })
        .then(response => response.json())
}
